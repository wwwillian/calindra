**Desafio Tech Calindra**

A desafio de Back-End e Front-End.

*Nesse repositório você vai encontrar ampos os teste. Teste de front-end foi feito com javascript seguindo a biblioteca react e escolhi o desafio numero dos 2. Pois no desafio 1 aparentemente o servidor da omelete estava com problema. Para o teste de banck-end optei ultizar o PHP com o framework laravel.*

---

## Download dos projetos

Os arquivos se encontram publico no Bitbucket, instalando o projeto.

1. Abra seu terminal.
2. Execute o comando $ git clone https://wwwillian@bitbucket.org/wwwillian/calindra.git
3. Execute o comando $ cd calindra
4. Siga os passos para o front-end ou o back-end

---

## Front-End

*Pré requisito ter o node instalado na maquina e npm ou yarn*

*Maiores informações sobre a instalação desses pacotes basta acessar:*
*Instalar node e npm https://nodejs.org/en/*
*Instalar yarn https://classic.yarnpkg.com/en/docs/cli/install*

5. $ cd front-end
6. $ yarn install *Esse comando instala as dependecias do projeto localizadas no arquivo package.json* ou $ npm install
7. $ yarn start ou $npm start

A partir dai o projeto já pode ser visualizado no navegador basta entrar com a url **http://localhost:3000**

---

## Back-End

**Instação e pacotes**

*Pré requisitos: php7.3 ou superior | composer | mysql | ferramenta de suporte para protocolos requisições*

*Sobre a instalação do php, mysql e composer - você pode pesquisar pois vai depender do sistema operacional - pode dar uma olhada na documentação https://www.php.net/downloads, mysql https://dev.mysql.com/doc/ e o composer https://getcomposer.org/*

*Se prefirir pode ultilizar o Docker windown - https://docs.docker.com/docker-for-windows/install ou MacOS - https://docs.docker.com/docker-for-mac/install/ ou Linux - https://www.digitalocean.com/community/tutorials/como-instalar-e-usar-o-docker-no-ubuntu-18-04-pt : após a instação você pode ultizar essa biblioteca que pode facilitar bastante a criação dos conteiner http://laradock.io/*

*Para a ferramenta de suporte para protocolos requisições - basta fazer download do Postman ou qualquer outra ferramenta: https://www.postman.com/downloads/*

**Com o ambiente preparado**

5. $ cd back-end  - ou se tiver na pasta do front-end $ cd ../back-end
6. $ composer install *Esse comando instala as dependecias do projeto localizadas no arquivo package.json*
7. $ cp .env.example .env *Abra seu editor e localize arquivo .env*

8. Crie uma tapela no banco de dados: caso precise de ajuda https://www.devmedia.com.br/primeiros-passos-no-mysql/28438

9. edite o .env

Exemplo: 

DB_CONNECTION=mysql *servidor do banco*
DB_HOST=mysql *host por padrão vai estar 127.0.0.1 banco se tiver com na maquina local nao tem necessidade de alterar*
DB_PORT=3306 *port padrao é 3306*
DB_DATABASE=calindra *tabela criada na etapa 8*
DB_USERNAME=root *o seu usuario do banco*
DB_PASSWORD= *e sua senha do banco de dados* 

***pode fechar o arquivo***

No terminal novamente
10. $ php artisan key:generate 
11. $ php artisan migrate
12. $ php artisan serve

Beleza projeto rodando 

Para visualizar as etapas do projeto

1. Na ferramenta de suporte para protocolos 
  1.1 POST - Add endereço no banco
    1.1.1 http://calindra.local/api/maps
    1.1.2 Heders -> Content-Type: application/json
    1.1.3 Body -> raw -> Json
    1.1.4 Criar um json com endereço para cadastro
      exemplo: {
        "address": "Av. Rio Branco, 1 ​ Centro, Rio de Janeiro ​ RJ, 20090​003"
      } 
    1.1.5 Send

Repetir esse processo 2 ou mais vezes alterando o endereço do JSON

1.2 GET - Mostra todos os endereços cadastrados - escolha os endereços para comparação - Anote o ID
  1.2.1 http://calindra.local/api/maps
  1.2.2 Heders -> Content-Type: application/json
  1.2.3 Send

1.3 GET - Implementar o algoritmo de cálculo de distância Euclidiana e salva
  1.3.1 http://calindra.local/euclidean/addressOne=*id endereço 1*/addressTwo=*id endereço 2*
  1.3.2 Heders -> Content-Type: application/json
  1.3.3 Send  

1.4 GET - Retorna os valores salvos
  1.4.1 http://calindra.local/euclidean
  1.4.2 Heders -> Content-Type: application/json
  1.4.3 Send  

****FIM**** 

Obs: Não esquece de deixa minha nota com 10... Brincadeira