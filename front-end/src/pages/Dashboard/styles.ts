import styled from 'styled-components';

import measuringEverything from '../../assets/measuring_everything.png';

export const MainContainer = styled.section`
  width: 100%;
  height: 1024px;
  background: #e9e9e9 url(${measuringEverything}) no-repeat 70% center;
  display: flex;
  align-items: center;
  .title {
    width: 50%;
    margin: auto;
    h1 {
      width: 348px;
      font-weight: 700;
      font-size: 82px;
      color: #716565;
    }
  }
`;
export const ContainerTest = styled.section`
  width: 100%;
  height: 1024px;
  left: 0px;
  background: rgba(80, 111, 169, 65%)
    linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4));
  img {
    position: absolute;
    margin-top: 100px;
    right: 0;
    z-index: -1;
  }
  .title {
    width: 40%;
    height: 840px;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    align-items: flex-end;
    h1 {
      width: 493px;
      font-weight: 700;
      font-size: 69.2px;
      color: #fff;
      margin-bottom: 20px;
    }
    p {
      width: 493px;
      font-style: normal;
      font-weight: 300;
      font-size: 48px;
      color: #fff;
    }
  }
`;
