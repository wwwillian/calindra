import React from 'react';

import Container from '../components/Container';
import CallToBanner from '../components/CallToBanner';
import CallToBannerBlue from '../components/CallToBannerBlue';

const Dashboard: React.FC = () => (
  <Container>
    <CallToBanner />
    <CallToBannerBlue />
  </Container>
);

export default Dashboard;
