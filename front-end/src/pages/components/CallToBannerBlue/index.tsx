import React from 'react';

import { CreateDivBannerBlue } from './styles';
import Illustration from '../../../assets/measure_tapes.svg';

const CallToBannerBlue: React.FC = () => (
  <CreateDivBannerBlue>
    <div className="area--info">
      <h1 className="title">We are Leader in Measure Tapes</h1>
      <p className="sub--title">
        There are 5x the circumference of planet earth in metric tapes.
      </p>
    </div>
    <div className="area--image">
      <img src={Illustration} className="image" alt="img_banner" />
    </div>
  </CreateDivBannerBlue>
);

export default CallToBannerBlue;
