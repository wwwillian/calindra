import styled from 'styled-components';

export const CreateDivBannerBlue = styled.div`
  background: #fff;
  position: relative;
  z-index: 1;
  display: grid;
  grid-template-areas: 'info image';
  height: 100vh;
  grid-template-columns: 1fr 1fr;

  @media (max-width: 939px) {
    height: 100vh;
    grid-template-columns: 1fr;
    grid-template-areas: 'info' 'image';
  }
  .title {
    max-width: 484px;
    font-size: 69.2px;
    font-weight: 700;
    margin-right: 5rem;
    margin-bottom: 3rem;
    color: #fff;

    @media (max-width: 1399px) and (min-width: 940px) {
      width: 684px;
      font-size: 58.2px;
      padding-left: 50px;
    }

    @media (max-width: 939px) and (min-width: 769px) {
      text-align: center;
      font-size: 48.2px;
      padding-top: 3rem;
      margin-right: 0;
    }
    @media (max-width: 768px) {
      font-size: 38.5px;
      padding: 4rem 1rem 1rem;
      margin-right: 0;
      text-align: center;
    }
  }
  .sub--title {
    max-width: 484px;
    font-size: 48.2px;
    margin-right: 5rem;
    font-weight: 300;
    margin-bottom: 10rem;
    color: #fff;

    @media (max-width: 1399px) and (min-width: 940px) {
      width: 584px;
      font-size: 38.2px;
      padding-left: 50px;
    }
    @media (max-width: 939px) and (min-width: 769px) {
      text-align: center;
      font-size: 28.2px;
      margin-right: 0;
    }
    @media (max-width: 768px) {
      padding: 0 1rem;
      text-align: center;
      font-size: 28.2px;
      margin-right: 0;
    }
  }
  .area--info {
    background: rgba(80, 111, 169, 90%)
      linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4));
    grid-area: info;
    display: flex;
    justify-content: flex-end;
    flex-direction: column;
    align-items: flex-end;

    @media (max-width: 939px) {
      align-items: center;
      justify-content: start;
    }
  }

  .area--image {
    background: rgba(80, 111, 169, 90%)
      linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4));
    grid-area: image;
    position: relative;
  }

  .image {
    position: absolute;
    right: 0;
    top: 10%;
    z-index: -1;
    @media (max-width: 939px) and (min-width: 769px) {
      width: 70%;
      margin-top: -30vh;
    }
    @media (max-width: 768px) {
      margin-top: -30vh;
      width: 80%;
    }
  }
`;
