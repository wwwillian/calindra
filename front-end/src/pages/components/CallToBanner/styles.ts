import styled from 'styled-components';

export const CreateDivBanner = styled.div`
  display: grid;
  grid-template-areas: 'info image';
  height: 100vh;
  grid-template-columns: 1fr 1fr;

  @media (max-width: 939px) {
    height: 100vh;
    grid-template-columns: 1fr;
    grid-template-areas: 'info' 'image';
  }

  .title {
    max-width: 348px;
    font-size: 82.2px;
    font-weight: 700;
    margin-right: 5rem;
    color: #716565;
    padding-top: 8rem;

    @media (max-width: 939px) and (min-width: 769px) {
      text-align: center;
      font-size: 62.2px;
      padding-top: 3rem;
      margin-right: 0;
    }
    @media (max-width: 768px) {
      font-size: 48.5px;
      padding-top: 4rem;
      margin-right: 0;
      text-align: center;
    }
  }

  .area--info {
    grid-area: info;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: flex-end;

    @media (max-width: 939px) {
      align-items: center;
    }
  }

  .area--image {
    grid-area: image;
    position: relative;
  }

  .image {
    position: absolute;
    left: 0;
    top: 38%;

    @media (max-width: 1388px) and (min-width: 939px) {
      top: 42%;
      width: 100%;
    }

    @media (max-width: 939px) and (min-width: 769px) {
      height: 30vh;
      margin-top: -20vh;
      left: 20%;
    }
    @media (max-width: 768px) {
      top: -20%;
      width: 90%;
      left: 5%;
    }
  }
`;
