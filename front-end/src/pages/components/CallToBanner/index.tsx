import React from 'react';

import { CreateDivBanner } from './styles';
import Illustration from '../../../assets/measuring_everything.png';

const CallToBanner: React.FC = () => (
  <CreateDivBanner>
    <div className="area--info">
      <h1 className="title">Measuring everything</h1>
    </div>
    <div className="area--image">
      <img src={Illustration} className="image" alt="img_banner" />
    </div>
  </CreateDivBanner>
);

export default CallToBanner;
