import React from 'react';

import { ContainerBlock } from './styles';

const Container: React.FC = ({children}) => (
  <ContainerBlock>
    {children}
  </ContainerBlock>
);

export default Container;
