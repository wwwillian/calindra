import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import GlobalStyle from './styles/global';
import Routes from './routes';

import NavMenu from './sections/NavMenu';
import FooterBlock from './sections/FooterBlock';

const App: React.FC = () => (
  <>
    <NavMenu />
    <BrowserRouter>
      <Routes />
    </BrowserRouter>
    <FooterBlock />
    <GlobalStyle />
  </>
);

export default App;
