import styled from 'styled-components';

export const Footer = styled.footer`
  width: 100%;
  height: 226px;

  position: relative;
  color: #846219;

  display: grid;
  grid-template-columns: 1.5fr 0.5fr;

  @media (max-width: 749px) {
    grid-template-columns: 1fr;
    grid-template-areas: 'info' 'contact';
  }

  .footer-block {
    grid-area: 'info';
    display: flex;
    align-items: center;
    justify-content: flex-end;
    @media (max-width: 749px) {
      margin-top: 2rem;
      justify-content: center;
      flex-wrap: wrap;
    }
    &__section,
    &__section-end {
      margin: 0 auto;
      width: 169px;
    }
    &__section-end {
      @media (max-width: 400px) {
        margin-bottom: 3rem;
      }
    }
    &__title {
      font-weight: 700;
      font-size: 27px;
      color: #716565;
      margin-bottom: 10px;
      @media (max-width: 749px) {
        margin-top: 2rem;
        text-align: center;
      }
    }

    &__social-networks {
      display: flex;
      justify-content: space-between;
    }

    &__text {
      grid-area: 'contact';
      font-weight: 400;
      color: #716565;
      @media (max-width: 749px) {
        text-align: center;
      }
    }
  }
`;
