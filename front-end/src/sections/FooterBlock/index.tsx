import React from 'react';

import logoYoutube from '../../assets/youtube.svg';
import logoTwitter from '../../assets/twitter.svg';
import logoFacebook from '../../assets/facebook.svg';
import { Footer } from './styles';

const FooterBlock: React.FC = () => (
  <Footer>
    <div className="footer-block">
      <section className="footer-block__section">
        <h6 className="footer-block__title">Follow Us</h6>
        <div className="footer-block__social-networks">
          <a href="/">
            <img src={logoYoutube} alt="logo_youtube" />
          </a>
          <a href="/">
            <img src={logoTwitter} alt="logo_youtube" />
          </a>
          <a href="/">
            <img src={logoFacebook} alt="logo_youtube" />
          </a>
        </div>
      </section>
      <section className="footer-block__section-end">
        <h6 className="footer-block__title">Contact</h6>
        <p className="footer-block__text">2490 Leisure Lane</p>
        <p className="footer-block__text">San Luis Obispo</p>
        <p className="footer-block__text">California</p>
      </section>
    </div>
    <div className="block-rigth" />
  </Footer>
);

export default FooterBlock;
