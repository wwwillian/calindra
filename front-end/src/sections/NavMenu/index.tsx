import React, { useState } from 'react';

import logoImg from '../../assets/logo.svg';
import menuBurger from '../../assets/menu.svg';
import menuClose from '../../assets/cancel.svg';
import { Header, MenuHamburger } from './styles';

const NavMenu: React.FC = () => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <Header>
        <div className="logo">
          <img src={logoImg} alt="logo" />
          <div className="logo__text">
            <span className="logo__text__name">zutterman</span>
            <span className="logo__text__slogan">Measure Tapes</span>
          </div>
        </div>
        <nav className="link">
          <ul>
            <li>
              <a href="/">ABOUT US</a>
            </li>
            <li>
              <a href="/">MODELS</a>
            </li>
            <li>
              <a href="/">GUARANTEE</a>
            </li>
          </ul>
        </nav>
      </Header>
      <MenuHamburger>
        <button type="button" className="menu" onClick={() => setOpen(!open)}>
          <img src={menuBurger} alt="logo" className={`open ${open}`} />
          <img src={menuClose} alt="logo" className={`closed ${open}`} />
        </button>
        <div className="logo">
          <img src={logoImg} alt="logo" className={`open-logo ${open}`} />
        </div>
        <nav className={`nav ${open}`}>
          <div className="logo">
            <img src={logoImg} alt="logo" />
            <div className="logo__text">
              <span className="logo__text__name">zutterman</span>
              <span className="logo__text__slogan">Measure Tapes</span>
            </div>
          </div>
          <ul>
            <li>
              <a href="/">ABOUT US</a>
            </li>
            <li>
              <a href="/">MODELS</a>
            </li>
            <li>
              <a href="/">GUARANTEE</a>
            </li>
          </ul>
        </nav>
      </MenuHamburger>
    </>
  );
};

export default NavMenu;
