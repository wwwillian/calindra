import styled from 'styled-components';

export const Header = styled.header`
  width: 100%;
  height: 110px;

  position: absolute;

  background-color: #ffffff;
  color: #846219;

  display: flex;
  align-items: center;
  justify-content: space-around;

  @media (max-width: 768px) {
    display: none;
  }

  img {
    width: 70px;
  }

  .logo {
    display: flex;
    &__text {
      display: flex;
      flex-direction: column;

      margin-left: 15px;

      &__name {
        font-size: 32px;
        font-weight: 700;
      }

      &__slogan {
        font-weight: 300;
        font-size: 18px;
      }
    }
  }

  .link {
    width: 30%;

    ul {
      display: flex;
      justify-content: space-between;

      li {
        list-style-type: none;

        a {
          text-decoration: none;
          color: #846219;
          font-weight: 300;
        }
      }
    }
  }
`;
export const MenuHamburger = styled.header`
  width: 100%;
  height: 60px;

  background-color: #ffffff;
  position: absolute;
  z-index: 1;

  display: flex;
  align-items: center;
  justify-content: center;

  @media (min-width: 768px) {
    display: none;
  }

  .menu {
    padding: 0 25px;
    border: none;
    background-color: transparent;
    position: absolute;
    z-index: 11;
    left: 0;
    img {
      height: 35px;
    }
    .open.true {
      display: none;
    }
    .closed.false {
      padding-left: -20px;
      display: none;
    }
  }

  .logo {
    margin: 0 auto;

    img {
      width: 40px;
    }
    .open-logo.true {
      display: none;
    }
  }

  .nav.true {
    background: #ffffff;
    transform: translateX(-100%, 0);
    height: 100vh;
    text-align: left;
    padding: 2rem;
    position: absolute;
    z-index: 10;
    top: 0;
    left: 0;
    transition: transform 0.1s ease-in-out;
    img {
      width: 70px;
    }
    .logo {
      display: flex;
      padding: 4rem 0;
      &__text {
        color: #846219;
        display: flex;
        flex-direction: column;
        margin-left: 15px;
        &__name {
          font-size: 32px;
          font-weight: 700;
        }
        &__slogan {
          font-weight: 300;
          font-size: 18px;
        }
      }
    }
    ul {
      height: 30vh;
      color: #846219;
      display: flex;
      flex-direction: column;
      justify-content: center;
      li {
        list-style-type: none;
        margin-bottom: 1rem;
        a {
          font-size: 1.8rem;
          text-decoration: none;
          color: #846219;
          font-weight: 300;
        }
      }
    }
  }
  .nav.false {
    transform: translateX(0%, -100%);
    display: none;
  }
`;
