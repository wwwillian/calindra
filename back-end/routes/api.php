<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/maps', 'App\Http\Controllers\AddressControllers\IndexController');
Route::get('/maps/{id}', 'App\Http\Controllers\AddressControllers\IndexController');
Route::post('/maps', 'App\Http\Controllers\AddressControllers\StoreController');

Route::get('/euclidean/addressOne={addressId}/addressTwo={addressIdTwo}',
  'App\Http\Controllers\EuclideanDistanceControllers\EuclideanCalculation');

Route::get('/euclidean', 'App\Http\Controllers\EuclideanDistanceControllers\IndexController');
