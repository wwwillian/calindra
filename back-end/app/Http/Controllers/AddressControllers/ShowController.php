<?php


namespace App\Http\Controllers\AddressControllers;

use App\Models\Address;
use App\Http\Controllers\Controller;

class ShowController extends Controller
{
  public function __invoke($id)
  {
    try {
      if ($id <= 0) {
        return response()->json(['message' => 'The value passed in the parameter cannot be greater than or equal to zero.']);
      }

      $address = Address::all()->find($id);

      if (!$address) {
        return response()->json(['message' => "Id $id - does not exist"]);
      }

      return response()->json($address, 200);

    } catch (\Exception $e) {

      return response()->json(['message' => $e->getMessage()], 500);
    }
  }
}
