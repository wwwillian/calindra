<?php


namespace App\Http\Controllers\AddressControllers;

use App\Models\Address;
use App\Http\Controllers\GoogleMapsControllers\GeolocationController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoreController extends Controller
{
  public function __invoke(Request $request)
  {
    try{
      $address = $request->address;

      $geolocation = new GeolocationController($address);

      $addresses = new Address;
      $addresses->street = $address;
      $addresses->latitude = $geolocation->index()->lat;
      $addresses->longitude = $geolocation->index()->lng;

      $addresses->save();

      return response()->json($addresses, 201);

    } catch (\Exception $e){

      return response()->json(['message' => $e->getMessage(), 500]);

    }
  }
}
