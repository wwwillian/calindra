<?php


namespace App\Http\Controllers\AddressControllers;

use App\Http\Controllers\Controller;
use App\Models\Address;

class IndexController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\JsonResponse
   */

  public function __invoke()
  {
    try {
      $address = Address::all();

      return response()->json($address, 200);

    } catch (\Exception $e) {

      return response()->json($e->getMessage(), 500);
    }
  }
}
