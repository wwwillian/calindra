<?php


namespace App\Http\Controllers\GoogleMapsControllers;

use App\Http\Controllers\Controller;

class GeolocationController extends Controller
{
  private $address;

  public function __construct(String $address)
  {
    $this->address = $address;
  }

  public function index()
  {
    $formatString = str_replace(" ", "%20", $this->address);
    $responseMap = new HttpMapsController($formatString);

    return $responseMap->index()->results[0]->geometry->location;
  }
}
