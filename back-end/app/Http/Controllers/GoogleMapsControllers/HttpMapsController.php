<?php


namespace App\Http\Controllers\GoogleMapsControllers;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;


class HttpMapsController extends Controller
{
  private $address;

  public function __construct(String $address)
  {
    $this->address = $address;
  }

  public function index()
  {
    $http = new Client();
    $http_response = $http->request('GET',
      "https://maps.googleapis.com/maps/api/geocode/json?address=$this->address&key=AIzaSyDwLWiUAvhPsNS69Y3DShVAFvronzM8bk0");

    return json_decode($http_response->getBody());
  }
}
