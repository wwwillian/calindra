<?php


namespace App\Http\Controllers\EuclideanDistanceControllers;

use App\Http\Controllers\Controller;
use App\Models\EuclideanDistance;

class IndexController extends Controller
{
  public function __invoke()
  {
    try {
      $euclidean = EuclideanDistance::all();

      return response()->json($euclidean, 200);

    } catch (\Exception $e) {

      return response()->json($e->getMessage(), 500);
    }
  }
}
