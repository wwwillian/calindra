<?php


namespace App\Http\Controllers\EuclideanDistanceControllers;

use App\Models\Address;
use App\Models\EuclideanDistance;
use App\Http\Controllers\Controller;

class EuclideanCalculation extends Controller
{
  public function __invoke($addressId, $addressIdTwo) {

    try {
      if ($addressId <= 0 && $addressIdTwo <= 0)
        return response()->json(['message' => 'The value passed in the parameter cannot be greater than or equal to zero.']);

      $address = Address::all()->find($addressId);
      $addressTwo = Address::all()->find($addressIdTwo);

      if (!$address && !$addressTwo)
        return response()->json(['message' => "Id $addressId or Id $addressIdTwo - does not exist"]);

      $latitudeOne = floatval($address->latitude);
      $longitudeOne = floatval($address->longitude);

      $latitudeTwo = floatval($addressTwo->latitude);
      $longitudeTwo = floatval($addressTwo->longitude);

      $distance = (sin(deg2rad($latitudeOne)) * sin(deg2rad($latitudeTwo))) +
        (cos(deg2rad($latitudeOne)) * cos(deg2rad($latitudeTwo)) * cos(deg2rad($longitudeOne - $longitudeTwo)));

      $distance = acos($distance);
      $distance = rad2deg($distance);
      $distance = $distance * 60 * 1.1515;
      $distance = $distance * 1.609344;
      $distance = round($distance,2);

      $saveCalculation = new EuclideanDistance;
      $saveCalculation->result = (string) $distance;
      $saveCalculation->address_one = $address->street;
      $saveCalculation->address_two = $addressTwo->street;
      $saveCalculation->save();

      return response()->json($saveCalculation, 201);

    } catch (\Exception $e) {

      return response()->json(['message' => $e->getMessage()], 500);
    }
  }
}
