<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EuclideanDistance extends Model
{
  use HasFactory;

  protected $fillable = [
    'result',
    'address_one',
    'address_two'
  ];
}
