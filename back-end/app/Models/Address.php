<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

  /**
   * An Eloquent Model: 'Address'
   *
   * @property int $id;
   * @property string $street;
   * @property string $latitude;
   * @property string $longitude;
   */
class Address extends Model
{
  use HasFactory;

  protected $fillable = [
    'street',
    'latitude',
    'longitude'
  ];
}
